package com.games24x7.mqtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.format.json.JSONObject;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.json.JSONArray;
import com.games24x7.framework.mq.MQFrameworkFactory;


/**
 * @author sanket
 * 
 */

public class CmMsgPublish

{
	private static Logger logger;
	private static PropsFileBasedConfiguration mainConfig;
	public static final String testPropsFile = "/home/sanket/workspace/CmBplMsgPublish/src/test/resources/mq.properties";
	public static void main( String[] args )
	{
		try
		{
			logger = LoggerFactory.getLogger( CmMsgPublish.class );
			mainConfig = new PropsFileBasedConfiguration( testPropsFile );
			String testQueue = "cmQueue";
			initMQ( testPropsFile, testQueue );
			int msgCount = 500;
			publishToMQ( testQueue, msgCount );
			System.exit( 0 );
			
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	private static void initMQ( String testPropsFile, String testQueue )
	{
		try
		{
			MQFrameworkFactory.init( new PropsFileBasedConfiguration( testPropsFile ) );
			System.out.println( "Initialized MQ Framework." );
		}
		catch( Exception e )
		{
			System.out.println( "Exception in initializing MQ Framework" );
		}
		try
		{
			MQFrameworkFactory.getFramework().registerQueuePublisher( testQueue );
			System.out.println( "Registered MQ Publisher." );
		}
		catch( Exception e )
		{
			System.out.println( "Exception in registering queue" );
		}
	}

	private static enum MessageType
	{
		RAPDESKTOP( "rap", 1 ),
		RAPMOBILE( "rap", 2 ),
		RAPAPK( "rap", 3 ),
		REG( "regmsg", 4 ), 
		FIRSTDEPOSIT( "firstdeposit", 5 ), 
		BONUSREG( "bonus_reg", 6 ), 
		FORGOTPWD( "forgotpwd", 7 ), 
		CHANGEMAILMSG( "changemailmsg", 7 ), 
		REPEATDEPOSIT("repeatdeposit", 9 ), 
		BONCRASHEMAIL( "boncrashemail",10 ), 
		BAFMSG( "bafmsg", 11 ), 
		EMARSYSUPDATE( "emarsysupdate", 12 ), 
		BAFREMINDERMSG( "bafremindermsg", 13 ), 
		DEPOSITLIMIT( "depositlimit", 14 ),
		NOMOBILEDSLIMIT( "depositlimit", 15 );

		private String testMessageName;
		private int testMessageIndex;

		private MessageType( String s, int i )
		{
			this.testMessageName = s;
			this.testMessageIndex = i;
		}

		public String getTestMessageName()
		{
			return this.testMessageName;
		}

		public int getTestMessageIndex()
		{
			return this.testMessageIndex;
		}
	}

	@SuppressWarnings( "deprecation" )
	private static void publishToMQ( String testQueue, int msgCount )
	{
		for( int i = 1; i <= 1; i++ )
		{
			for( MessageType messageType : MessageType.values() )
			{
				try
				{
					if (mainConfig.getBooleanValue( "allemail" )==true)
					{
						if( messageType.getTestMessageIndex() !=100 )
						{
							JSONObject message = getJSON( messageType );
							if( message != null )
							MQFrameworkFactory.getFramework().publishToQueue( testQueue, message.toString() );
							logger.debug( "Published Message for " + messageType.getTestMessageName() + ", count " + i );
							Thread.sleep( 2000 ); // 2 sec Wait time

						}
						
					}
					else if (messageType.getTestMessageIndex() == mainConfig.getIntValue( "messagetype"))
					{
						JSONObject message = getJSON( messageType );
						if( message != null )
						MQFrameworkFactory.getFramework().publishToQueue( testQueue, message.toString() );
						logger.debug( "Published Message for " + messageType.getTestMessageName() + ", count " + i );
						Thread.sleep( 2000 ); // 2 sec Wait time

					}
						
					
				}
				catch( Exception e )
				{
					System.out.println( "Exception while publishing msg " + messageType.getTestMessageName() + ", count " + i );
				}
			}
		}
	}

	private static JSONObject getJSON( MessageType messageType )
	{
		JSONObject message = null;
		String crashrefund = "{\"systime\":1475648417269,\"id\":1,\"dest\":\"CM\",\"source\":\"55550\",\"ackreq\":0,\"value\":{\"players\":[{\"amount\":10,\"userpreferredlanguage\":1,\"userid\":10320},{\"amount\":10,\"userpreferredlanguage\":1,\"userid\":17335}],\"tournamentid\":12479270,\"errmsg\":\"crashandrefund\"},\"type\":\"boncrashemail\"}";
		String emarsyupdate = "{\"dest\":\"cm\",\"source\":\"fm\",\"value\":{\"players\":[{\"amount\":500,\"entryfee\":250,\"userid\":26295},{\"amount\":0,\"entryfee\":250,\"userid\":271963}],\"settlementtype\":3,\"prizetype\":1},\"type\":\"emarsysupdate\"}";
		try
		{
			message = new JSONObject();
			if( messageType.getTestMessageIndex() == 1 )
			{
				// RAP-DESKTOP
				
				message.put( "systime", System.currentTimeMillis() );
				message.put( "tablenum", 0 );
				message.put( "msgid", 4 );
				message.put( "clientvariant", 1 );
				message.put( "settlementtype", 2 );
				message.put( "tournamentname", "" );
				message.put( "matchid", 0 );
				message.put( "roundnumber", 0 );
				message.put( "playerid", 17335 );
				message.put( "type", "rap" );
				message.put( "sessionid", "SSID63d71fe7-46ee-4b0b-aec9-1563433e1fe3" );
				message.put( "gameid", 0 );
				message.put( "dealnum", 0 );
				message.put( "category", "Disconnection" );
				message.put( "dest", "TE" );
				message.put( "source", 17335 );
				message.put( "ackreq", 0 );
				message.put( "fakemsg", false );
				message.put( "ptext", "Test msg for Desktop" + messageType.getTestMessageName() + ", please ignore..." );
				message.put( "tournamentid", 12479231 );
				message.put( "istiebreaker", false );
				message.put( "receiverid", 3301972 );
				message.put( "classid", 1048602 );
				logger.debug( "Email Sent For RAP Desktop" );
			}
			else if( messageType.getTestMessageIndex() == 2 )
			{
				// RAP-MOBILE

				message.put( "systime", System.currentTimeMillis() );
				message.put( "tablenum", 0 );
				message.put( "msgid", 4 );
				message.put( "clientvariant", 2 );
				message.put( "settlementtype", 2 );
				message.put( "tournamentname", "" );
				message.put( "matchid", 0 );
				message.put( "roundnumber", 0 );
				message.put( "playerid", 17335 );
				message.put( "type", "rap" );
				message.put( "sessionid", "SSID63d71fe7-46ee-4b0b-aec9-1563433e1fe3" );
				message.put( "gameid", 0 );
				message.put( "dealnum", 0 );
				message.put( "category", "Disconnection" );
				message.put( "dest", "TE" );
				message.put( "source", 17335 );
				message.put( "ackreq", 0 );
				message.put( "fakemsg", false );
				message.put( "ptext", "Test msg for Mobile " + messageType.getTestMessageName() + ", please ignore..." );
				message.put( "tournamentid", 12479231 );
				message.put( "istiebreaker", false );
				message.put( "receiverid", 3301972 );
				message.put( "classid", 1048602 );
				logger.debug( "Email Sent For RAP Mobile" );
				
			}
			else if( messageType.getTestMessageIndex() == 3 )
			{
				// RAP-APK

				message.put( "systime", System.currentTimeMillis() );
				message.put( "tablenum", 0 );
				message.put( "msgid", 4 );
				message.put( "clientvariant", 3 );
				message.put( "settlementtype", 2 );
				message.put( "tournamentname", "" );
				message.put( "matchid", 0 );
				message.put( "roundnumber", 0 );
				message.put( "playerid", 17335 );
				message.put( "type", "rap" );
				message.put( "sessionid", "SSID63d71fe7-46ee-4b0b-aec9-1563433e1fe3" );
				message.put( "gameid", 0 );
				message.put( "dealnum", 0 );
				message.put( "category", "Disconnection" );
				message.put( "dest", "TE" );
				message.put( "source", 17335 );
				message.put( "ackreq", 0 );
				message.put( "fakemsg", false );
				message.put( "ptext", "Test msg for APK" + messageType.getTestMessageName() + ", please ignore..." );
				message.put( "tournamentid", 12479231 );
				message.put( "istiebreaker", false );
				message.put( "receiverid", 3301972 );
				message.put( "classid", 1048602 );
				logger.debug( "Email Sent For RAP Apk" );
				
			}

			else if( messageType.getTestMessageIndex() == 4 )
			{
				// Registartion Email Trigger
				message.put( "datetime", 0 );
				message.put( "userpreferredlanguage", 1 );
				message.put( "loginid", "cmtest2" );
				message.put( "emailid", mainConfig.getStringValue( "email" ));
				message.put( "spamcheckrequired", false );
				message.put( "time", System.currentTimeMillis() );
				message.put( "type", "regmsg" );
				message.put( "emarsysswitch", "ON" );
				message.put( "userid", 361604 );
				message.put( "spamstatus", 0 );
				logger.debug( "Email Sent For Registartion" );
			}
			else if( messageType.getTestMessageIndex() == 5 )
			{
				// First Deposit Email trigger
				message.put( "userpreferredlanguage", 1 );
				message.put( "spamstatus", 0 );
				message.put( "userid", 387816 );
				message.put( "loginid", "cmtest1" );
				message.put( "type", "firstdeposit" );
				message.put( "withdrawable", 100.0 );
				message.put( "time", System.currentTimeMillis() );
				message.put( "paymentmode", "NETBANKING" );
				message.put( "bonusamount", 0 );
				message.put( "depositamount", 100.0 );
				message.put( "email", mainConfig.getStringValue( "email" ) );
				message.put( "playername", "cmtest3" );
				message.put( "spamcheckrequired", false );
				message.put( "orderid", "LK7YUSN2N0" );
				logger.debug( "Email Sent For First Deposit" );
			}
			else if( messageType.getTestMessageIndex() == 6 )
			{
				// Bonus Reg Email Trigger
				message.put( "amount", 200 );
				message.put( "datetime", System.currentTimeMillis() );
				message.put( "userpreferredlanguage", 1 );
				message.put( "loginid", "salt" );
				message.put( "emailid", mainConfig.getStringValue( "email" ) );
				message.put( "time", System.currentTimeMillis() );
				message.put( "spamcheckrequired", false );
				message.put( "type", "bonus_reg" );
				message.put( "emarsysswitch", "ON" );
				message.put( "userid", 17335 );
				message.put( "spamstatus", 0 );
				logger.debug( "Email Sent For Bonus Reg" );
			}
			else if( messageType.getTestMessageIndex() == 7 )
			{
				// Forgot pwd Email Trigger
				message.put( "userpreferredlanguage", 0 );
				message.put( "loginid", "salt" );
				message.put( "spamcheckrequired", false );
				message.put( "time", System.currentTimeMillis() );
				message.put( "type", "forgotpwd" );
				message.put( "emarsysswitch", "ON" );
				message.put( "userid", 17335 );
				message.put( "email", mainConfig.getStringValue( "email" ) );
				message.put( "spamstatus", 0 );
				message.put( "token", "eab191f9-ea7a-4095-8674-d93f156853ad" );
				logger.debug( "Email Sent For Forgot Password" );
			}
			else if( messageType.getTestMessageIndex() == 8 )
			{
				// Change email msg Email Trigger
				message.put( "userpreferredlanguage", 1 );
				message.put( "loginid", "salt" );
				message.put( "emailid", mainConfig.getStringValue( "email" ) );
				message.put( "spamcheckrequired", false );
				message.put( "time", System.currentTimeMillis() );
				message.put( "type", "changemailmsg" );
				message.put( "emarsysswitch", "ON" );
				message.put( "userid", 17335 );
				message.put( "spamstatus", 0 );
				logger.debug( "Email Sent For Change Email" );
			}
			else if( messageType.getTestMessageIndex() == 9 )
			{
				// Repeat deposit Email Trigger
				message.put( "userpreferredlanguage", 1 );
				message.put( "spamstatus", 0 );
				message.put( "userid", 387815 );
				message.put( "loginid", "cmtest1" );
				message.put( "type", "repeatdeposit" );
				message.put( "withdrawable", 200.0 );
				message.put( "mobileno", "8776567527" );
				message.put( "time", System.currentTimeMillis() );
				message.put( "paymentmode", "NETBANKING" );
				message.put( "bonusamount", 0 );
				message.put( "depositamount", 100.0 );
				message.put( "email", mainConfig.getStringValue( "email" ) );
				JSONObject gametiles = new JSONObject();
				gametiles.put( "tile2_players", 2 );
				gametiles.put( "tile1_value1", 0.05 );
				gametiles.put( "tile1_value3", 4 );
				gametiles.put( "tile1_header", "Points Rummy" );
				gametiles.put( "tile1_players", 2 );
				gametiles.put( "tile2_value1", 25 );
				gametiles.put( "tile2_header", "Pool Rummy" );
				gametiles.put( "tile2_cta_url", "https://try.rummycircle.com/player/playNowThroughEmail.html?formatType=201&ptValue=0&player=2&eFee=25" );
				gametiles.put( "tile1_text1", "Point Value" );
				gametiles.put( "tile1_text3", "Buy-In" );
				gametiles.put( "tile2_value3", "101 Pool" );
				gametiles.put( "tile2_text1", "Entry fee" );
				gametiles.put( "tile1_cta_url", "https://try.rummycircle.com/player/playNowThroughEmail.html?formatType=100&ptValue=0.05&player=2&eFee=4" );
				gametiles.put( "tile2_text3", "Game Type" );
				message.put( "gametiles", gametiles );
				message.put( "spamcheckrequired", false );
				message.put( "orderid", "LK7YU33EPZ" );
				logger.debug( "Email Sent For Repeat Deposit" );
			}
			else if( messageType.getTestMessageIndex() == 10 )
			{
				// Crash refund Email Trigger
				JSONObject crashandrefund = new JSONObject( crashrefund );
				message = crashandrefund;
				logger.debug( "Email Sent For Crash Refund" );
				
			}

			else if( messageType.getTestMessageIndex() == 11 )
			{
				// Bafmsg Email Trigger
				message.put( "sendername", "BafEmail" );
				message.put( "userpreferredlanguage", 1 );
				message.put( "recipientemail", mainConfig.getStringValue( "email" ) );
				message.put( "loginid", "sanket" );
				message.put( "rummystarstatus", 0 );
				message.put( "emailservice", "other" );
				message.put( "recipientname", "test0011" );
				message.put( "promocodeid", 50839346 );
				message.put( "spamcheckrequired", false );
				message.put( "type", "bafmsg" );
				message.put( "lpurl", "null" );
				message.put( "userid", 387912 );
				message.put( "spamstatus", 0 );
				message.put( "emailtemplateid_reg", 0 );
				message.put( "invitationstatus", 2 );
				message.put( "personalmsg", "hi" );
				message.put( "refid", "https://www.rummycircle.com/?refId=387912" );
				message.put( "time", System.currentTimeMillis() );
				message.put( "emailtemplateid_nonreg", 0 );
				message.put( "usercreationtime", System.currentTimeMillis() );
				message.put( "ismobile", true );
				logger.debug( "Email Sent For Baf Message" );

			}

			else if( messageType.getTestMessageIndex() == 12 )
			{
				// Emarsys update Email Trigger
				JSONObject emarsysupdate = new JSONObject( emarsyupdate );
				message = emarsysupdate;
				logger.debug( "Email Sent For Emarsys Update" );
			}

			else if( messageType.getTestMessageIndex() == 13 )
			{
				// Baf reminder msg Email Trigger
				message.put( "sendername", "BafEmailReminder" );
				message.put( "userpreferredlanguage", 1 );
				message.put( "recipientemail", mainConfig.getStringValue( "email" ) );
				message.put( "loginid", "sanket" );
				message.put( "rummystarstatus", 0 );
				message.put( "emailservice", "others" );
				message.put( "recipientname", "test0011" );
				message.put( "joinedstatus", "N" );
				message.put( "promocodeid", 50839346 );
				message.put( "spamcheckrequired", false );
				message.put( "type", "bafremindermsg" );
				message.put( "lpurl", "null" );
				message.put( "emarsysswitch", "ON" );
				message.put( "userid", 387912 );
				message.put( "spamstatus", 0 );
				message.put( "cashstatus", "N" );
				message.put( "paymentstatus", "N" );
				message.put( "invitationstatus", 2 );
				message.put( "refid", "https://www.rummycircle.com/?refId=387912" );
				message.put( "time", System.currentTimeMillis() );
				message.put( "usercreationtime", System.currentTimeMillis() );
				logger.debug( "Email Sent For BAF Reminder" );
			}
			else if( messageType.getTestMessageIndex() == 14 )
			{
				// Deposit limit with mobile Email Trigger
				message.put( "userpreferredlanguage", 1 );
				message.put( "currentlimit", 125000 );
				message.put( "loginid", "febfusion001" );
				message.put( "groupid", "null" );
				message.put( "mobile", "8527712211" );
				message.put( "available", 120000 );
				message.put( "requestdate", date() );
				message.put( "spamcheckrequired", false );
				message.put( "type", "depositlimit" );
				message.put( "userid", 340795 );
				message.put( "spamstatus", 0 );
				message.put( "maxlimit", 125000 );
				message.put( "limittype", "M" );
				message.put( "accountcreationdate", "null" );
				message.put( "idverified", "N" );
				message.put( "time", System.currentTimeMillis()  );
				message.put( "email", mainConfig.getStringValue( "email" ) );
				logger.debug( "Email Sent For Deposit limit with mobile" );
			}
			else if( messageType.getTestMessageIndex() == 15 )
			{
				// Deposit limit without mobile Email Trigger
				message.put( "userpreferredlanguage", 1 );
				message.put( "currentlimit", 125000 );
				message.put( "loginid", "febfusion001" );
				message.put( "groupid", "null" );
				message.put( "mobile", "" );
				message.put( "available", 120000 );
				message.put( "requestdate", date() );
				message.put( "spamcheckrequired", false );
				message.put( "type", "depositlimit" );
				message.put( "userid", 340795 );
				message.put( "spamstatus", 0 );
				message.put( "maxlimit", 125000 );
				message.put( "limittype", "M" );
				message.put( "accountcreationdate", "null" );
				message.put( "idverified", "N" );
				message.put( "time", System.currentTimeMillis()  );
				message.put( "email", mainConfig.getStringValue( "email" ) );
				logger.debug( "Email Sent For Deposit limit without mobile" );
			}

		}
		catch( Exception e )
		{
			e.printStackTrace();
		}

		return message;
	}
	
	public static boolean date ()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Calendar cal = Calendar.getInstance();
		System.out.println(dateFormat.format(cal.getTime()));
		return false;
	}
}
