package com.games24x7.mqtest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.games24x7.format.json.JSONObject;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;

public class BplMsgPublish
{

	private static PropsFileBasedConfiguration mainConfig;
	public static final String testPropsFile = "/home/sanket/workspace/CmBplMsgPublish/src/test/resources/mq.properties";

	public static void main( String[] args )
	{
		try
		{
			mainConfig = new PropsFileBasedConfiguration( testPropsFile );
			String testQueue = "bpltv";
			initMQ( testPropsFile, testQueue );
			int msgCount = 500;
			publishToMQ( testQueue, msgCount );
			System.exit( 0 );

		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	private static void initMQ( String testPropsFile, String testQueue )
	{
		try
		{
			MQFrameworkFactory.init( new PropsFileBasedConfiguration( testPropsFile ) );
			System.out.println( "Initialized MQ Framework." );
		}
		catch( Exception e )
		{
			System.out.println( "Exception in initializing MQ Framework" );
		}
		try
		{
			MQFrameworkFactory.getFramework().registerQueuePublisher( testQueue );
			System.out.println( "Registered MQ Publisher." );
		}
		catch( Exception e )
		{
			System.out.println( "Exception in registering queue" );
		}
	}

	private static enum MessageType
	{
		BPL( "bpl", 1 );

		private String testMessageName;
		private int testMessageIndex;

		private MessageType( String s, int i )
		{
			this.testMessageName = s;
			this.testMessageIndex = i;
		}

		public String getTestMessageName()
		{
			return this.testMessageName;
		}

		public int getTestMessageIndex()
		{
			return this.testMessageIndex;
		}
	}

	@SuppressWarnings( "deprecation" )
	private static void publishToMQ( String testQueue, int msgCount )
	{
		for( int i = 1; i <= 1; i++ )
		{
			for( MessageType messageType : MessageType.values() )
			{
				try
				{
					if( messageType.getTestMessageIndex() == 1 )
					{
						JSONObject message = getJSON( messageType );
						if( message != null )

							MQFrameworkFactory.getFramework().publishToQueue( testQueue, message.toString() );
						System.out.println( "Published msg " + messageType.getTestMessageName() + ", count " + i );
						Thread.sleep( 2000 ); // 2 sec
									// Wait
									// time

					}
				}
				catch( Exception e )
				{
					System.out.println( "Exception while publishing msg " + messageType.getTestMessageName() + ", count " + i );
				}
			}
		}
	}

	private static JSONObject getJSON( MessageType messageType )
	{
		int Entryfee,winner,amount,revenuecontribution;
		int totalbuyin=mainConfig.getIntValue( "totalbuyin" );
		Entryfee =totalbuyin+totalbuyin;
		winner=Entryfee*10/100;
		revenuecontribution=winner/2;
		amount= Entryfee-winner;
		//System.out.println("Result"+ Entryfee);
		//System.out.println( amount );
		//System.out.println(winner);
		//System.out.println(revenuecontribution);
		int userid=mainConfig.getIntValue( "userid" );
		JSONObject message = null;
		String bpl = "{\"player\":[{\"amount\":"+amount+",\"revenuecontribution\":"+revenuecontribution+",\"winner\":true,\"playerid\":"+userid+",\"totalbuyin\":"+totalbuyin+",\"updaterole\":true,\"channelid\":1},{\"amount\":0,\"revenuecontribution\":"+revenuecontribution+",\"winner\":false,\"playerid\":10320,\"totalbuyin\":"+totalbuyin+",\"updaterole\":false,\"channelid\":2}],\"settlementtype\":3,\"revenueper\":10,\"matchid\":13807517,\"aaid\":3302644,\"revenueeach\":\""+revenuecontribution+"\",\"tid\":12480022,\"type\":\"bpl\",\"prizetype\":2}";
		try
		{
			message = new JSONObject();
			if( messageType.getTestMessageIndex() == 1 )
			{
				// RAP-DESKTOP

				JSONObject bafamount = new JSONObject( bpl );
				message = bafamount;
				System.out.println( "Message to Bpl"+bpl );
			}

		}
		catch( Exception e )
		{
			e.printStackTrace();
		}

		return message;
	}

	public static boolean date()
	{
		DateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS" );
		Calendar cal = Calendar.getInstance();
		System.out.println( dateFormat.format( cal.getTime() ) );
		return false;
	}
	

}
